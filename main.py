from concurrent import futures
import time
import grpc
from dynaconf import settings
from services.http_server import HTTPServer

from services.account_manager import AccountManager, \
    add_AccountManagerServicer_to_server
from services.artist_manager import ArtistManager, \
    add_UserArtistManagerServicer_to_server
from services.concert_manager import ConcertManager, \
    add_ConcertStreamerServicer_to_server
from services.airline_provider import AirlineTicketProvider, \
    add_AirlineTicketProviderServicer_to_server
from services.order_manager import OrderManager, \
    add_OrderManagerServicer_to_server

from backports.datetime_fromisoformat import MonkeyPatch
MonkeyPatch.patch_fromisoformat()


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    add_AccountManagerServicer_to_server(AccountManager(), server)
    add_UserArtistManagerServicer_to_server(ArtistManager(), server)
    add_ConcertStreamerServicer_to_server(ConcertManager(), server)
    add_AirlineTicketProviderServicer_to_server(
        AirlineTicketProvider(), server
    )
    add_OrderManagerServicer_to_server(OrderManager(), server)
    server.add_insecure_port(settings.LISTEN_ON)
    server.start()

    http_server = HTTPServer(settings.HTTP_LISTEN_ON)
    http_server.start()

    try:
        while True:
            time.sleep(5)
    except KeyboardInterrupt:
        server.stop(0)
        http_server.stop()


if __name__ == '__main__':
    serve()
