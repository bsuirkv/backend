from services.repositories import UserRepository


def get_user(func):
    def _request_user(instance, request, context):
        metadata = dict(context.invocation_metadata())
        uid = metadata['uid'] if 'uid' in metadata else None
        if uid is None:
            raise ValueError('User is required')

        user = UserRepository.find_by_telegram_uid(uid)
        if user is None:
            raise ValueError('User was not found')

        return func(instance, request, context, user)

    return _request_user
