from .database import BaseModel
from .music import Concert
from .travel import Flight
from .user import User
from peewee import IntegerField, CharField, ForeignKeyField, BooleanField


class Ticket(BaseModel):
    for_type = CharField()
    for_id = IntegerField()
    url = CharField()


class Order(BaseModel):
    user = ForeignKeyField(User, backref='orders')
    concert = ForeignKeyField(Concert)
    flight = ForeignKeyField(Flight, null=True)
    is_paid = BooleanField()


class OrderTicket(BaseModel):
    order = ForeignKeyField(Order, backref='tickets')
    ticket = ForeignKeyField(Ticket)
