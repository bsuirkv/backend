from .database import BaseModel
from peewee import DoesNotExist  # noqa: F401
from peewee import CharField, IntegerField, FloatField


class User(BaseModel):
    class Meta:
        indexes = (
            (('source', 'source_id'), True),
        )

    source = CharField()
    source_id = IntegerField()
    first_name = CharField()
    last_name = CharField()
    home_city_name = CharField(null=True)
    home_city_lat = FloatField(null=True)
    home_city_lng = FloatField(null=True)
