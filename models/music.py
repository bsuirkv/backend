import dateutil.parser
import datetime
import pytz
from .database import BaseModel
from peewee import DoesNotExist  # noqa: F401
from peewee import IntegerField, CharField, ForeignKeyField, DateTimeField, \
    FloatField, TextField, BooleanField
from .user import User


class ArtistPriority:
    UNDEFINED = 0
    LOW = 1
    HIGH = 2

    @classmethod
    def is_valid(cls, value):
        return value in [cls.HIGH, cls.LOW]


class Artist(BaseModel):
    songkick_id = IntegerField(unique=True)
    songkick_uri = CharField()
    name = CharField()


class UserArtist(BaseModel):
    user = ForeignKeyField(User, backref='artists')
    artist = ForeignKeyField(Artist)
    priority = IntegerField()

    def set_priority(self, new_priority):
        if ArtistPriority.is_valid(new_priority):
            self.priority = new_priority


class Concert(BaseModel):
    songkick_id = IntegerField()
    songkick_uri = CharField()
    linked_artist = ForeignKeyField(Artist, backref='concerts')
    type = CharField()
    name = CharField()
    start_time = DateTimeField()
    city_name = CharField()
    city_lat = FloatField()
    city_lng = FloatField()
    venue = TextField()
    performances = TextField()
    status = CharField()
    age_restriction = CharField()

    def get_start_datetime(self):
        if isinstance(self.start_time, datetime.datetime):
            return pytz.utc.localize(self.start_time)

        try:
            return dateutil.parser.parse(self.start_time)
        except AttributeError:
            datetime.datetime.strptime(self.start_time, '%Y-%m-%d %H:%M:%S')


class UserConcertNotification(BaseModel):
    user = ForeignKeyField(User, backref='notifications')
    concert = ForeignKeyField(Concert)
    is_sent = BooleanField()
    distance = FloatField(null=True)
