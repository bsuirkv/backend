import json
import dateutil.parser
import datetime

from models.music import Concert, Artist
from songkiss.models.event import Event

import messages.base_types_pb2


class DictEncoder(json.JSONEncoder):
    def default(self, o):
        return o.__dict__


class ConcertTransition:
    @staticmethod
    def songkick_event_to_entity(
            songkick_event: Event,
            artist: Artist
    ) -> Concert:
        entity = Concert()

        entity.songkick_id = songkick_event.id
        entity.songkick_uri = songkick_event.uri
        entity.type = songkick_event.mtype
        entity.status = songkick_event.status
        entity.name = songkick_event.display_name
        entity.age_restriction = str(songkick_event.age_restriction)

        entity.performances = json.dumps(
            songkick_event.performance,
            cls=DictEncoder
        )
        entity.venue = json.dumps(
            songkick_event.venue,
            cls=DictEncoder
        )

        entity.city_name = songkick_event.location.city
        entity.city_lat = songkick_event.location.lat
        entity.city_lng = songkick_event.location.lng

        if songkick_event.start.datetime is not None:
            entity.start_time = dateutil.parser.parse(
                songkick_event.start.datetime
            )
        else:
            entity.start_time = datetime.datetime.now()

        entity.linked_artist = artist

        return entity

    @staticmethod
    def deserialize_venue(serialized_venue: str) \
            -> messages.base_types_pb2.Venue:
        venue_data = json.loads(serialized_venue)

        venue_city = messages.base_types_pb2.City(
            id=venue_data['metro_area']['id'],
            name=venue_data['metro_area']['display_name'],
            country=venue_data['metro_area']['country']['display_name'],
            uri=venue_data['metro_area']['uri']
        )

        venue = messages.base_types_pb2.Venue(
            id=venue_data['id'],
            name=venue_data['display_name'],
            uri=venue_data['uri'],
            city=venue_city
        )

        return venue

    @staticmethod
    def deserialize_performances(
            serialized_performances: str,
            proto_performances: messages.base_types_pb2.Performance
    ):
        performance_list = json.loads(serialized_performances)
        for performance in performance_list:
            proto_performances.add(
                id=performance['id'],
                name=performance['display_name'],
                billing=performance['billing'],
                billingIndex=performance['billing_index'],
                artist=messages.base_types_pb2.Artist(
                    artistId=performance['artist']['id'],
                    artistName=performance['artist']['display_name'],
                    artistUri=performance['artist']['uri']
                )
            )

    @classmethod
    def entity_to_proto_concert(cls, concert: Concert) \
            -> messages.base_types_pb2.Concert:
        start_time = concert.start_time if isinstance(
            concert.start_time, str
        ) else concert.start_time.isoformat()

        proto_concert = messages.base_types_pb2.Concert(
            concertId=concert.id,
            concertType=concert.type,
            concertUri=concert.songkick_uri,
            name=concert.name,
            start=start_time,
            end='',
            status=concert.status,
            ageRestriction=concert.age_restriction,
            venue=cls.deserialize_venue(concert.venue),
            location=messages.base_types_pb2.Location(
                city=concert.city_name,
                lat=concert.city_lat,
                lng=concert.city_lng
            )
        )

        cls.deserialize_performances(
            concert.performances,
            proto_concert.performances
        )

        return proto_concert


class FlightTransition:
    @staticmethod
    def flight_entity_to_message_keys(flight):
        flight_message_keys = {
            'id': flight.id,
            'origin': flight.origin,
            'destination': flight.destination,
            'departureDate': flight.departure_date,
            'returnDate': flight.return_date,
            'numberOfChanges': flight.number_of_changes,
            'price': int(flight.price)
        }

        return flight_message_keys


class TicketTransition:
    @staticmethod
    def ticket_entity_to_message_keys(ticket):
        ticket_message_keys = {
            'id': ticket.id,
            'forType': ticket.for_type,
            'forId': ticket.for_id,
            'url': ticket.url
        }

        return ticket_message_keys


class OrderTransition:
    @staticmethod
    def order_entity_to_message(order):
        order_message_keys = {
            'id': order.id,
            'concert': ConcertTransition.entity_to_proto_concert(
                order.concert
            ),
        }

        order_message = messages.base_types_pb2.Order(**order_message_keys)

        if order.flight is not None:
            order_message.flights.add(
                **FlightTransition.flight_entity_to_message_keys(
                    order.flight
                )
            )

        if len(order.tickets) > 0:
            for order_ticket in order.tickets:
                order_message.tickets.add(
                    **TicketTransition.ticket_entity_to_message_keys(
                        order_ticket.ticket
                    )
                )

        return order_message
