from .database import BaseModel
from .user import User
from peewee import CharField, IntegerField, BooleanField, DateTimeField, \
    DecimalField, ForeignKeyField


class Flight(BaseModel):
    origin = CharField()
    destination = CharField()
    departure_date = DateTimeField()
    return_date = DateTimeField()
    number_of_changes = IntegerField()
    price = DecimalField()
    is_return = BooleanField()


class FlightReservation(BaseModel):
    user = ForeignKeyField(User)
    flight = ForeignKeyField(Flight)
    reserved_at = DateTimeField()
    valid_for = IntegerField()
