from peewee import SqliteDatabase, Model

db = SqliteDatabase('people.db')


class BaseModel(Model):
    class Meta:
        database = db
