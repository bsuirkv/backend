import requests
from dynaconf import settings


class AviasalesAPI:
    CURRENCY_EUR = 'eur'

    def __init__(self, api_key: str = settings.AVIASALES_API_KEY):
        self.api_key = api_key

    @staticmethod
    def get_city_airport(city_name):
        r = requests.get(
            'https://autocomplete.travelpayouts.com/places2?{}'.format(
                '&'.join([
                    'term={}'.format(city_name),
                    'locale=en',
                    'types[]=city,airport'
                ])
            )
        )

        cities_json = r.json()
        if len(cities_json) == 0:
            return None

        return cities_json[0]['code']

    def get_prices(self, from_city, to_city, date, currency=None):
        if currency is None:
            currency = self.CURRENCY_EUR

        url_params = [
            'currency={}'.format(currency),
            'origin={}'.format(from_city),
            'destination={}'.format(to_city),
            'beginning_of_period={:%Y-%m-%d}'.format(date),
            'show_to_affiliates=false',
            'trip_duration=1',
            'token={}'.format(self.api_key)
        ]

        r = requests.get(
            'https://api.travelpayouts.com/v2/prices/latest?{}'.format(
                '&'.join(url_params)
            )
        )

        return r.json()['data']
