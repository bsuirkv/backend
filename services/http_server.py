import threading
import http.server
from urllib.parse import urlparse, parse_qs
from services.spotifier import Spotifier


class HTTPServer:
    def __init__(self, listen_on: str = '127.0.0.1:8300'):
        self.thread = threading.Thread(target=self._run)
        self.thread.daemon = True
        self.http_server = None

        listen_parts = listen_on.split(':')
        if len(listen_parts) != 2:
            raise ValueError('listen_on should be in host:port format!')

        self.listen_on = (listen_parts[0], int(listen_parts[1]))

    def _run(self):
        self.http_server = http.server.HTTPServer(
            self.listen_on, SpotifyHTTPRequestHandler
        )
        self.http_server.serve_forever()

    def start(self):
        self.thread.start()

    def stop(self):
        if self.http_server is not None:
            self.http_server.shutdown()


class SpotifyHTTPRequestHandler(http.server.BaseHTTPRequestHandler):
    SPOTIFY_REDIRECT_PATH = '/spotify/'
    SPOTIFY_CALLBACK_PATH = '/spotify_callback'

    def __init__(self, request, client_address, server):
        self.spotify = Spotifier()
        super().__init__(request, client_address, server)

    def do_GET(self):
        if self.path.startswith(self.SPOTIFY_REDIRECT_PATH):
            uid = self.path.replace(self.SPOTIFY_REDIRECT_PATH, '')
            try:
                uid = int(uid)
                if uid < 1:
                    raise ValueError()
            except ValueError:
                self.send_response(404)
                self.end_headers()
                return

            self.send_response(302)
            self.send_header('Location', self.spotify.get_url_for_uid(uid))
            self.end_headers()
        elif self.path.startswith(self.SPOTIFY_CALLBACK_PATH):
            parsed_url = urlparse(self.path)
            query = parse_qs(parsed_url.query)

            if 'code' not in query or 'state' not in query:
                self.send_response(404)
                self.end_headers()

            try:
                token = self.spotify.get_token_by_code(
                    query['code'][0],
                    query['state'][0]
                )
                self.spotify.handle_import(token)
            except ValueError as error:
                self.send_response(400)
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.wfile.write(str(error).encode())

                return

            self.send_response(302)
            self.send_header('Location', 'tg://null')

            self.end_headers()
        else:
            self.send_response(404)
            self.end_headers()
