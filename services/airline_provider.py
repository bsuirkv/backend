import messages.travel_pb2
import datetime
from decorators.helpers import get_user
from services.aviasales import AviasalesAPI
from models.transitions import FlightTransition
from services.repositories import FlightRepository, FlightReservationRepository
from services.proto.travel_pb2_grpc import AirlineTicketProviderServicer

from services.proto.travel_pb2_grpc import add_AirlineTicketProviderServicer_to_server  # noqa: F401, E501


class AirlineTicketProvider(AirlineTicketProviderServicer):
    def __init__(self):
        self.aviasales = AviasalesAPI()

    @get_user
    def GetAvailableFlights(self, request, context, user=None):
        from_city = request.fromCity
        if not from_city:
            from_city = user.home_city_name

        currency = request.currency if request.currency else None

        from_airport = self.aviasales.get_city_airport(from_city)
        to_airport = self.aviasales.get_city_airport(request.toCity)

        departure_date = datetime.datetime.strptime(
            request.departureDate, '%Y-%m-%d'
        )

        tickets = self.aviasales.get_prices(
            from_airport,
            to_airport,
            departure_date,
            currency
        )

        response = messages.travel_pb2.AvailableFlightsResponse()

        for ticket in tickets:
            ticket_entity = FlightRepository.create_for_aviasales_flight(
                ticket
            )
            response.flights.add(
                **FlightTransition.flight_entity_to_message_keys(ticket_entity)
            )

        return response

    @get_user
    def ReserveFlight(self, request, context, user=None):
        flight = FlightRepository.get_by_id(request.flightId)
        reservation = FlightReservationRepository.create_for_user_and_flight(
            user,
            flight
        )

        response = messages.travel_pb2.FlightReservationResponse(
            reservationId=reservation.id,
            validFor=reservation.valid_for
        )

        return response
