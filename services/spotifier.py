import hmac
import requests
from models.user import User, DoesNotExist
from models.music import Artist
from services.repositories import ArtistRepository, UserRepository, \
    UserArtistRepository
from services.notificator import Notificator

from songkiss.songkiss_client import SongkissClient
from dynaconf import settings
import spotipy

from typing import Optional


class HMACUidSigner:
    def __init__(self, secret_key: str = settings.HMAC_SECRET):
        self.hmac = hmac.new(secret_key.encode())

    def generate_for_uid(self, uid: int) -> str:
        uid_str = str(uid)
        self.hmac.update(uid_str.encode())

        return '{}:{}'.format(uid_str, self.hmac.hexdigest())

    def parse_uid(self, hmacode: str) -> int:
        hmacode_parts = hmacode.split(':')
        if len(hmacode_parts) != 2:
            raise ValueError('Invalid hmacode format')

        self.hmac.update(hmacode_parts[0].encode())
        valid_hex_digest = self.hmac.hexdigest()

        if not hmac.compare_digest(valid_hex_digest, hmacode_parts[1]):
            raise ValueError('Invalid hmacode')

        return int(hmacode_parts[0])


class Spotifier:
    RESPONSE_TYPE_CODE = 'code'

    PERM_TOP_READ = 'user-top-read'
    PERM_READ_EMAIL = 'user-read-email'

    GRANT_AUTH_CODE = 'authorization_code'

    SPOTIFY_ACCOUNT_HOST = 'https://accounts.spotify.com'

    def __init__(
            self,
            client_id: str = settings.SPOTIFY_CLIENT_ID,
            client_secret: str = settings.SPOTIFY_CLIENT_SECRET,
            callback_url: str = settings.SPOTIFY_CALLBACK_URL,
            spotify_host: Optional[str] = None
    ):
        self.client_id = client_id
        self.client_secret = client_secret
        self.host = self.SPOTIFY_ACCOUNT_HOST
        if spotify_host is not None:
            self.host = spotify_host
        self.hmacoder = HMACUidSigner()
        self.callback_url = callback_url
        self.songkick = SongkissClient(settings.SONGKICK_KEY)
        self.notificator = Notificator()

    def get_url_for_uid(self, uid: int) -> str:
        url_params = [
            'client_id={}'.format(self.client_id),
            'response_type={}'.format(self.RESPONSE_TYPE_CODE),
            'redirect_uri={}'.format(self.callback_url),
            'scope={}'.format(' '.join(
                [self.PERM_TOP_READ, self.PERM_READ_EMAIL]
            )),
            'state={}'.format(self.hmacoder.generate_for_uid(uid))
        ]

        url = '{}/authorize?{}'.format(
            self.host,
            '&'.join(url_params)
        )

        return url

    def get_token_by_code(self, code: str, hmacode: str):
        uid = self.hmacoder.parse_uid(hmacode)

        token_response = requests.post('{}/api/token'.format(self.host), {
            'grant_type': self.GRANT_AUTH_CODE,
            'code': code,
            'redirect_uri': self.callback_url
        }, auth=(self.client_id, self.client_secret))

        token_json = token_response.json()
        if 'access_token' in token_json:
            return {
                'uid': uid,
                'token': token_json['access_token'],
                'expires_in': token_json['expires_in'],
                'refresh_token': token_json['refresh_token']
            }
        else:
            raise ValueError('Error getting access_token')

    def handle_import(self, full_token):
        user = UserRepository.get_by_id(int(full_token['uid']))
        spotify = spotipy.Spotify(auth=full_token['token'])
        top_artists = spotify.current_user_top_artists(limit=10)

        added_artists = []
        for artist in top_artists['items']:
            added_artist = self._find_and_add(artist['name'], user)
            if added_artist is not None:
                added_artists.append(added_artist)

    def _find_and_add(self, artist_name: str, user: User) -> Optional[Artist]:
        artist = ArtistRepository.get_or_none_by_name(artist_name)
        if artist is not None:
            try:
                connection = UserArtistRepository.find_by_user_and_artist(
                    user, artist
                )
            except DoesNotExist:
                connection = UserArtistRepository.create(user, artist)

            self.notificator.create_notifications_for_connection(connection)

            return artist

        artists_response = self.songkick.search.get_search_artists(
            artist_name
        )
        added_artist = None

        if artists_response.results_page.results is not None:
            for artist in artists_response.results_page.results.artist:
                artist_entity = ArtistRepository.\
                    create_for_songkick_if_not_exists(artist)

                if artist_entity.name != artist_name:
                    continue

                try:
                    connection = UserArtistRepository.find_by_user_and_artist(
                        user, artist
                    )
                except DoesNotExist:
                    connection = UserArtistRepository.create(user, artist)

                self.notificator.create_notifications_for_connection(
                    connection
                )
                added_artist = artist_entity

        return added_artist
