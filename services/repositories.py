import datetime

from models.music import Artist, UserArtist, ArtistPriority, Concert, \
    UserConcertNotification
from models.travel import Flight, FlightReservation
from models.orders import Order, OrderTicket
from models.user import User

from typing import Iterable, Optional, List
from songkiss.models.artist_1 import Artist1


class UserRepository:
    SOURCE_TG = 'telegram'

    @classmethod
    def get_by_telegram_uid(cls, uid: int) -> User:
        return User.get(
            (User.source_id == uid) & (User.source == cls.SOURCE_TG)
        )

    @classmethod
    def find_by_telegram_uid(cls, uid: int) -> Optional[User]:
        return User.get_or_none(
            (User.source_id == uid) & (User.source == cls.SOURCE_TG)
        )

    @classmethod
    def create_telegram_user(
            cls,
            uid: int,
            first_name: str,
            last_name: str
    ) -> User:
        user = User.create(
            source=cls.SOURCE_TG,
            source_id=uid,
            first_name=first_name,
            last_name=last_name
        )
        user.save()

        return user

    @staticmethod
    def get_all() -> Iterable[User]:
        return User.select()

    @staticmethod
    def get_by_id(pk):
        return User.get_by_id(pk)


class ArtistRepository:
    @staticmethod
    def get_all() -> Iterable[Artist]:
        return Artist.select()

    @staticmethod
    def get_by_id(row_id: int) -> Artist:
        return Artist.get_by_id(row_id)

    @staticmethod
    def get_by_songkick_id(songkick_id: int) -> Artist:
        return Artist.get(Artist.songkick_id == songkick_id)

    @staticmethod
    def get_or_none_by_name(artist_name: str) -> Optional[Artist]:
        return Artist.get_or_none(Artist.name == artist_name)

    @staticmethod
    def create_for_songkick_if_not_exists(songkick_artist: Artist1) -> Artist:
        artist, created = Artist.get_or_create(
            songkick_id=songkick_artist.id,
            songkick_uri=songkick_artist.uri,
            name=songkick_artist.display_name
        )

        return artist


class UserArtistRepository:
    @staticmethod
    def create(user: User, artist: Artist) -> UserArtist:
        user_artist_relation = UserArtist(
            user=user,
            artist=artist,
            priority=ArtistPriority.HIGH
        )
        user_artist_relation.save()

        return user_artist_relation

    @staticmethod
    def find_by_user_and_artist(user: User, artist: Artist) -> UserArtist:
        return UserArtist.get(
            (UserArtist.user == user) & (UserArtist.artist == artist)
        )

    @staticmethod
    def get_active() -> Iterable[UserArtist]:
        return UserArtist.select().where(
            UserArtist.priority == ArtistPriority.HIGH
        )

    @staticmethod
    def get_active_for_user(user: User) -> Iterable[UserArtist]:
        return UserArtist.select().where(
            (UserArtist.priority == ArtistPriority.HIGH)
            & (UserArtist.user == user)
        )


class ConcertRepository:
    @staticmethod
    def get_by_id(pk: int) -> Concert:
        return Concert.get_by_id(pk)

    @staticmethod
    def get_by_songkick_id(sk_id: int) -> Concert:
        return Concert.get(Concert.songkick_id == sk_id)

    @staticmethod
    def get_for_artists_before(artists: List[Artist], before: str):
        return Concert.select().join(Artist).where(
            Concert.linked_artist.in_(artists),
            Concert.start_time < before
        ).order_by(Artist.name, Concert.start_time)


class UserConcertNotificationRepository:
    @staticmethod
    def get_or_none_by_user_and_concert(
            user: User,
            concert: Concert
    ) -> Optional[UserConcertNotification]:
        return UserConcertNotification.get_or_none(
            (UserConcertNotification.user == user)
            & (UserConcertNotification.concert == concert)
        )

    @staticmethod
    def create_by_user_and_concert(
            user: User,
            concert: Concert,
            distance: float
    ) -> UserConcertNotification:
        user_concert_notification = UserConcertNotification(
            user=user,
            concert=concert,
            distance=distance,
            is_sent=False
        )
        user_concert_notification.save()

        return user_concert_notification

    @staticmethod
    def after(pk: int) -> Iterable[UserConcertNotification]:
        return UserConcertNotification.select(
            UserConcertNotification.user
        ).distinct().where(
            (UserConcertNotification.id >= pk)
            & (UserConcertNotification.is_sent == False)  # noqa: E712
        )

    @staticmethod
    def get_for_user(user: User) -> Iterable[UserConcertNotification]:
        return UserConcertNotification.select().where(
            (UserConcertNotification.user == user)
            & (UserConcertNotification.is_sent == False)  # noqa: E712
        ).order_by(UserConcertNotification.distance.asc())

    @staticmethod
    def read_all(notifications: List[UserConcertNotification]):
        notification_ids = [notification.id for notification in notifications]
        UserConcertNotification.update(is_sent=True).where(
            UserConcertNotification.id.in_(notification_ids)
        ).execute()


class FlightRepository:
    @staticmethod
    def get_by_id(pk):
        return Flight.get_by_id(pk)

    @staticmethod
    def create_for_aviasales_flight(aviasales_flight):
        flight = Flight()
        flight.origin = aviasales_flight['origin']
        flight.destination = aviasales_flight['destination']
        flight.departure_date = aviasales_flight['depart_date']
        flight.return_date = aviasales_flight['return_date']
        flight.number_of_changes = aviasales_flight['number_of_changes']
        flight.price = aviasales_flight['value']
        flight.is_return = True

        flight.save()

        return flight


class FlightReservationRepository:
    @staticmethod
    def get_by_id(pk):
        return FlightReservation.get_by_id(pk)

    @staticmethod
    def create_for_user_and_flight(user, flight):
        reservation = FlightReservation(
            user=user,
            flight=flight,
            reserved_at=datetime.datetime.now(),
            valid_for=1000000
        )
        reservation.save()

        return reservation


class OrderRepository:
    @staticmethod
    def get_by_id(pk):
        return Order.get_by_id(pk)

    @staticmethod
    def get_for_user(user):
        return Order.select().where(Order.user == user)

    @staticmethod
    def create_for_user_and_concert(user, concert):
        order = Order(
            user=user,
            concert=concert,
            is_paid=False
        )
        order.save()

        return order


class OrderTicketRepository:
    @staticmethod
    def create_for_order_and_ticket(order, ticket):
        order_ticket = OrderTicket(
            order=order,
            ticket=ticket
        )
        order_ticket.save()

        return order_ticket
