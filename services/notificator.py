import datetime
import geopy.distance
import pytz

from models.music import UserArtist, Concert, User
from services.repositories import UserConcertNotificationRepository \
    as NotifyRepository

DISTANCE_KM_THRESHOLD = 1000


class Notificator:
    @staticmethod
    def get_concert_distance(concert: Concert, user: User) -> float:
        concert_coordinate = (concert.city_lat, concert.city_lng)
        user_coordinate = (user.home_city_lat, user.home_city_lng)

        distance = geopy.distance.distance(user_coordinate, concert_coordinate)

        return distance.km

    @staticmethod
    def is_nearby(distance: float):
        return distance < DISTANCE_KM_THRESHOLD

    @staticmethod
    def now() -> datetime.datetime:
        return datetime.datetime.now(pytz.utc)

    def create_notifications_for_connection(
            self,
            user_artist_connection: UserArtist
    ) -> int:
        created = 0

        for artist_concert in user_artist_connection.artist.concerts:
            concert_distance = self.get_concert_distance(
                artist_concert, user_artist_connection.user
            )

            if not self.is_nearby(concert_distance):
                continue

            if artist_concert.get_start_datetime() <= self.now():
                continue

            notification = NotifyRepository.get_or_none_by_user_and_concert(
                user_artist_connection.user,
                artist_concert
            )
            if notification is None:
                NotifyRepository.create_by_user_and_concert(
                    user_artist_connection.user,
                    artist_concert,
                    concert_distance
                )
                created += 1

        return created
