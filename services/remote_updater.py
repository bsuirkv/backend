from models.music import Artist, DoesNotExist
from models.transitions import ConcertTransition
from services.repositories import ConcertRepository
from songkiss.songkiss_client import SongkissClient
from dynaconf import settings


class RemoteUpdater:
    def __init__(self):
        self.songkick = SongkissClient(settings.SONGKICK_KEY)

    def update_concerts_for_artist(self, artist: Artist) -> int:
        calendar = self.songkick.artists.get_artist_calendar(
            artist.songkick_id
        )
        if calendar.results_page.results is None:
            return 0

        created = 0
        for concert in calendar.results_page.results.event:
            try:
                ConcertRepository.get_by_songkick_id(concert.id)
            except DoesNotExist:
                concert_entity = ConcertTransition.songkick_event_to_entity(
                    concert, artist
                )
                concert_entity.save()
                created += 1

        return created
