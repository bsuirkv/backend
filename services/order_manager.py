from decorators.helpers import get_user
from services.proto.orders_pb2_grpc import OrderManagerServicer
from services.repositories import OrderRepository, ConcertRepository, \
    FlightReservationRepository, OrderTicketRepository
from services.ticket_issuer import TicketIssuer
from services.notificator import Notificator

from messages.orders_pb2 import OrderCreatedResponse, PaidOrderResponse, \
    OrderListResponse
from models.transitions import OrderTransition, ConcertTransition

from services.proto.orders_pb2_grpc import add_OrderManagerServicer_to_server  # noqa: F401, E501


class OrderManager(OrderManagerServicer):
    def __init__(self):
        self.ticket_issuer = TicketIssuer()

    @get_user
    def CreateOrder(self, request, context, user=None):
        concert = ConcertRepository.get_by_id(request.concertId)
        order = OrderRepository.create_for_user_and_concert(
            user, concert
        )

        if len(request.flightReservationIds) > 0:
            reservation = FlightReservationRepository.get_by_id(
                request.flightReservationIds[0]
            )
            order.flight = reservation.flight
            order.save()

        return OrderCreatedResponse(
            order=OrderTransition.order_entity_to_message(order)
        )

    @get_user
    def PayForOrder(self, request, context, user=None):
        order = OrderRepository.get_by_id(request.orderId)
        if order.user.id != user.id:
            raise ValueError('This order doesn\'t belong to current user')

        order.is_paid = True
        order.save()

        distance = Notificator.get_concert_distance(order.concert, user)
        concert_ticket = self.ticket_issuer.issue_for_concert(
            ConcertTransition.entity_to_proto_concert(order.concert),
            distance
        )

        OrderTicketRepository.create_for_order_and_ticket(
            order, concert_ticket
        )

        return PaidOrderResponse(
            order=OrderTransition.order_entity_to_message(
                OrderRepository.get_by_id(request.orderId)
            )
        )

    @get_user
    def ListOrders(self, request, context, user=None):
        order_messages = [
            OrderTransition.order_entity_to_message(o) for o in user.orders
        ]

        response = OrderListResponse()
        response.orders.extend(order_messages)

        return response
