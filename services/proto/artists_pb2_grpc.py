# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
# flake8: noqa
import grpc

import messages.artists_pb2 as artists__pb2


class UserArtistManagerStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.SearchArtistByName = channel.unary_unary(
        '/UserArtistManager/SearchArtistByName',
        request_serializer=artists__pb2.SearchArtistByNameRequest.SerializeToString,
        response_deserializer=artists__pb2.FoundArtistsReply.FromString,
        )
    self.AddArtistToUser = channel.unary_unary(
        '/UserArtistManager/AddArtistToUser',
        request_serializer=artists__pb2.ArtistIdRequest.SerializeToString,
        response_deserializer=artists__pb2.ArtistOperationStatusReply.FromString,
        )
    self.DeleteArtistFromUser = channel.unary_unary(
        '/UserArtistManager/DeleteArtistFromUser',
        request_serializer=artists__pb2.ArtistIdRequest.SerializeToString,
        response_deserializer=artists__pb2.ArtistOperationStatusReply.FromString,
        )
    self.GetUserArtistInfo = channel.unary_unary(
        '/UserArtistManager/GetUserArtistInfo',
        request_serializer=artists__pb2.GetUserArtistInfoRequest.SerializeToString,
        response_deserializer=artists__pb2.UserArtistInfoReply.FromString,
        )
    self.SetArtistPriority = channel.unary_unary(
        '/UserArtistManager/SetArtistPriority',
        request_serializer=artists__pb2.ArtistPriorityRequest.SerializeToString,
        response_deserializer=artists__pb2.ArtistOperationStatusReply.FromString,
        )
    self.GetUserArtistList = channel.unary_unary(
        '/UserArtistManager/GetUserArtistList',
        request_serializer=artists__pb2.GetUserArtistListRequest.SerializeToString,
        response_deserializer=artists__pb2.UserArtistListReply.FromString,
        )


class UserArtistManagerServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def SearchArtistByName(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def AddArtistToUser(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def DeleteArtistFromUser(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetUserArtistInfo(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SetArtistPriority(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetUserArtistList(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_UserArtistManagerServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'SearchArtistByName': grpc.unary_unary_rpc_method_handler(
          servicer.SearchArtistByName,
          request_deserializer=artists__pb2.SearchArtistByNameRequest.FromString,
          response_serializer=artists__pb2.FoundArtistsReply.SerializeToString,
      ),
      'AddArtistToUser': grpc.unary_unary_rpc_method_handler(
          servicer.AddArtistToUser,
          request_deserializer=artists__pb2.ArtistIdRequest.FromString,
          response_serializer=artists__pb2.ArtistOperationStatusReply.SerializeToString,
      ),
      'DeleteArtistFromUser': grpc.unary_unary_rpc_method_handler(
          servicer.DeleteArtistFromUser,
          request_deserializer=artists__pb2.ArtistIdRequest.FromString,
          response_serializer=artists__pb2.ArtistOperationStatusReply.SerializeToString,
      ),
      'GetUserArtistInfo': grpc.unary_unary_rpc_method_handler(
          servicer.GetUserArtistInfo,
          request_deserializer=artists__pb2.GetUserArtistInfoRequest.FromString,
          response_serializer=artists__pb2.UserArtistInfoReply.SerializeToString,
      ),
      'SetArtistPriority': grpc.unary_unary_rpc_method_handler(
          servicer.SetArtistPriority,
          request_deserializer=artists__pb2.ArtistPriorityRequest.FromString,
          response_serializer=artists__pb2.ArtistOperationStatusReply.SerializeToString,
      ),
      'GetUserArtistList': grpc.unary_unary_rpc_method_handler(
          servicer.GetUserArtistList,
          request_deserializer=artists__pb2.GetUserArtistListRequest.FromString,
          response_serializer=artists__pb2.UserArtistListReply.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'UserArtistManager', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
