import messages.artists_pb2
import messages.base_types_pb2
import services.proto.artists_pb2_grpc
from decorators.helpers import get_user
from services.repositories import ArtistRepository, UserArtistRepository

from songkiss.songkiss_client import SongkissClient
from peewee import DoesNotExist

from services.proto.artists_pb2_grpc import add_UserArtistManagerServicer_to_server  # noqa: F401, E501


class ArtistManager(services.proto.artists_pb2_grpc.UserArtistManagerServicer):
    def __init__(self):
        self.songkick = SongkissClient('9IMeg5LOsjXPGqRH')

    def SearchArtistByName(self, request, context):
        artists_response = self.songkick.search.get_search_artists(
            request.artistName
        )
        artists_found_message = messages.artists_pb2.FoundArtistsReply()

        for artist in artists_response.results_page.results.artist:
            artist_entity = ArtistRepository.create_for_songkick_if_not_exists(
                artist
            )
            artists_found_message.artists.add(
                artistId=artist_entity.id,
                artistName=artist.display_name,
                artistUri=artist.uri
            )

        return artists_found_message

    @get_user
    def AddArtistToUser(self, request, context, user=None):
        try:
            artist = ArtistRepository.get_by_id(request.artistId)
            try:
                UserArtistRepository.find_by_user_and_artist(user, artist)
            except DoesNotExist:
                UserArtistRepository.create(user, artist)

            return messages.artists_pb2.ArtistOperationStatusReply(
                success=True
            )
        except DoesNotExist:
            return messages.artists_pb2.ArtistOperationStatusReply(
                success=False
            )

    @get_user
    def DeleteArtistFromUser(self, request, context, user=None):
        return messages.artists_pb2.ArtistOperationStatusReply(
            success=False
        )

    @get_user
    def SetArtistPriority(self, request, context, user=None):
        try:
            artist = ArtistRepository.get_by_id(request.artistId)
            user_artist_relation = UserArtistRepository.\
                find_by_user_and_artist(user, artist)
            user_artist_relation.set_priority(request.priorityId)
            user_artist_relation.save()

            return messages.artists_pb2.ArtistOperationStatusReply(
                success=True
            )
        except DoesNotExist:
            return messages.artists_pb2.ArtistOperationStatusReply(
                success=False
            )

    @get_user
    def GetUserArtistInfo(self, request, context, user=None):
        try:
            artist = ArtistRepository.get_by_id(request.artistId)
            user_artist_relation = UserArtistRepository.\
                find_by_user_and_artist(user, artist)

            artist_message = messages.base_types_pb2.Artist(
                artistId=artist.id,
                artistName=artist.name,
                artistUri=artist.songkick_uri
            )
            response = messages.artists_pb2.UserArtistInfoReply(
                artist=artist_message,
                artistPriority=user_artist_relation.priority
            )

            return response
        except DoesNotExist:
            raise ValueError('Unknown artist')

    @get_user
    def GetUserArtistList(self, request, context, user=None):
        reply = messages.artists_pb2.UserArtistListReply()
        for connection in UserArtistRepository.get_active_for_user(user):
            reply.artists.add(
                artistId=connection.artist.id,
                artistName=connection.artist.name,
                artistUri=connection.artist.songkick_uri
            )

        return reply
