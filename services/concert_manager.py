import datetime

import messages.concerts_pb2
import services.proto.concerts_pb2_grpc
from services.repositories import UserConcertNotificationRepository, \
    UserArtistRepository, ConcertRepository
from services.notificator import Notificator
from models.transitions import ConcertTransition
from decorators.helpers import get_user

from services.proto.concerts_pb2_grpc import add_ConcertStreamerServicer_to_server  # noqa: F401, E501


class ConcertManager(services.proto.concerts_pb2_grpc.ConcertStreamerServicer):
    def GetScheduledConcerts(self, request, context):
        for notification in UserConcertNotificationRepository.after(
                request.after
        ):
            user = notification.user

            response = messages.concerts_pb2.ScheduledConcertResponse(
                telegramUid=user.source_id
            )

            for user_notification in UserConcertNotificationRepository.\
                    get_for_user(user):
                response.concert.add(
                    distance=notification.distance,
                    concert=ConcertTransition.entity_to_proto_concert(
                        user_notification.concert
                    )
                )

            yield response

    @get_user
    def GetUserScheduledConcerts(self, request, context, user=None):
        user_artists = [
            conn.artist for conn in UserArtistRepository.get_active_for_user(
                user
            )
        ]

        before = request.before
        if request.before == '':
            before = self._get_today_plus_year().isoformat()

        concerts = ConcertRepository.get_for_artists_before(
            user_artists, before
        )

        reply = messages.concerts_pb2.ConcertsScheduledForUserReply()
        for concert in concerts:
            reply.concert.add(
                distance=Notificator.get_concert_distance(concert, user),
                concert=ConcertTransition.entity_to_proto_concert(concert)
            )

        return reply

    @staticmethod
    def _get_today_plus_year():
        return datetime.datetime.utcnow() + datetime.timedelta(days=365 * 2)
