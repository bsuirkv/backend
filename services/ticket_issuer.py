import string
import random
import datetime

from models.orders import Ticket

from api2pdf import Api2Pdf
from dynaconf import settings


class TicketIssuer:
    def __init__(self):
        self.a2p_client = Api2Pdf(settings.API2PDF_KEY)

    def issue_for_concert(self, concert, distance):
        ticket = Ticket(
            for_type='concert',
            for_id=concert.concertId,
            url=self.generate_pdf_for_concert(concert, distance)
        )
        ticket.save()

        return ticket

    def generate_pdf_for_concert(self, concert, distance):
        start_datetime = datetime.datetime.fromisoformat(concert.start)

        ticket_template = string.Template(
            open('templates/concert_ticket.html').read()
        )
        ticket_code = ticket_template.substitute(**{
            'artist_name': concert.performances[0].artist.artistName,
            'location': '{}, {}'.format(
                concert.venue.name, concert.location.city
            ),
            'start_date': '{:%A %d %B %Y}'.format(start_datetime),
            'price': '{}'.format(int(20 + distance / 100)),
            'qrkey': random.random()
        })

        api_response = self.a2p_client.HeadlessChrome.convert_from_html(
            ticket_code
        )
        pdf_url = api_response.result['pdf']

        return pdf_url
