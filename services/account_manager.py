import messages.account_pb2
import services.proto.account_pb2_grpc
from services.repositories import UserRepository
from decorators.helpers import get_user
from services.http_server import SpotifyHTTPRequestHandler

from geopy.geocoders import Nominatim
from peewee import DoesNotExist
from dynaconf import settings

from services.proto.account_pb2_grpc import add_AccountManagerServicer_to_server  # noqa: F401, E501

APP_USER_AGENT = 'x-telegram/t.me/gigbot'


class AccountManager(services.proto.account_pb2_grpc.AccountManagerServicer):
    GEO_LOCATOR_LANGUAGE = 'en'

    def __init__(self):
        self.geo_locator = Nominatim(user_agent=APP_USER_AGENT)
        self.http_host = settings.HTTP_HOST

    def RegisterUser(self, request, context):
        try:
            UserRepository.get_by_telegram_uid(request.uid)

            return messages.account_pb2.UserRegisteredReply(success=False)
        except DoesNotExist:
            UserRepository.create_telegram_user(
                request.uid,
                request.firstName,
                request.lastName
            )

            return messages.account_pb2.UserRegisteredReply(success=True)

    def SetHomeCity(self, request, context):
        user = UserRepository.get_by_telegram_uid(request.uid)
        location = self._get_city_by_coordinates(
            request.location.lat, request.location.lng
        )

        user.home_city_lat = request.location.lat
        user.home_city_lng = request.location.lng
        user.home_city_name = self._get_city_name_by_location(location)
        user.save()

        return messages.account_pb2.HomeCitySetReply(
            cityName=user.home_city_name
        )

    @get_user
    def GetSpotifyAuthUrl(self, request, context, user=None):
        return messages.account_pb2.SpotifyAuthUrlReply(
            spotifyAuthUrl='{}{}{}'.format(
                self.http_host,
                SpotifyHTTPRequestHandler.SPOTIFY_REDIRECT_PATH,
                user.id
            )
        )

    def _get_city_by_coordinates(self, lat, lng):
        return self.geo_locator.reverse(
            ', '.join([str(lat), str(lng)]),
            language=self.GEO_LOCATOR_LANGUAGE
        )

    @staticmethod
    def _get_city_name_by_location(location):
        return ', '.join([
            location.raw['address']['city'],
            location.raw['address']['country']
        ])
