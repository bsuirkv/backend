import unittest
import grpc_testing
import grpc
import services.account_manager
from messages import account_pb2 as account__pb2

from peewee import SqliteDatabase
from models.user import User


test_db = SqliteDatabase(':memory:')


class AccountManagerTest(unittest.TestCase):
    def setUp(self):
        self.servicer = services.account_manager.AccountManager()
        self.time = grpc_testing.strict_real_time()
        self.descriptors = account__pb2._ACCOUNTMANAGER.methods_by_name
        self.server = grpc_testing.server_from_dictionary({
                account__pb2._ACCOUNTMANAGER: self.servicer
            },
            self.time
        )

        test_db.bind([User], bind_refs=True, bind_backrefs=True)

        test_db.connect()
        test_db.create_tables([User])

    def tearDown(self):
        test_db.drop_tables([User])
        test_db.close()

    def test_register(self):
        client = self.server.invoke_unary_unary(
            self.descriptors['RegisterUser'],
            (),
            account__pb2.RegisterUserRequest(uid=12),
            1
        )
        response, trailing_metadata, code, details = client.termination()
        self.assertEqual(grpc.StatusCode.OK, code)
