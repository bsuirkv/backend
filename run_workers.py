import workers.concert_importer

if __name__ == '__main__':
    concert_importer_worker = workers.concert_importer.ConcertImporter()
    concert_importer_worker.run()
