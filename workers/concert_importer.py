from services.repositories import ArtistRepository, UserArtistRepository
from services.notificator import Notificator
from services.remote_updater import RemoteUpdater


class ConcertImporter:
    def __init__(self):
        self.remote_updater = RemoteUpdater()
        self.notificator = Notificator()

    def run(self):
        for artist in ArtistRepository.get_all():
            self.remote_updater.update_concerts_for_artist(artist)

        for user_artist_connection in UserArtistRepository.get_active():
            self.notificator.create_notifications_for_connection(
                user_artist_connection
            )
