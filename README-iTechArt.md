# GigBot backend
Please note files in `messages/` and `services/proto/` are auto-generated from .proto definitions and thus ignored from flake8 code style analysis. We are using [pre-commit](https://pre-commit.com/) for managing pre-commit hooks (including running flake8).

We are *planning* on adding unit tests for critical code (we are considering critial all of `services/*_manager.py`).

We are **using** cloud CI/CD service [Buddy](https://buddy.works/) for building docker images and will be using it as well for running tests. We would *not* configure a CI job to run tests on each commit since 120 executions/month is quite low for executing on each commit, we would run tests manually when building a docker image instead.

We *did not use* any branching strategies since we split our work by services.
