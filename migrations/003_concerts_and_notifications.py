"""Peewee migrations -- 003_concerts_and_notifications.py.

Some examples (model - class or model name)::

    > Model = migrator.orm['model_name']            # Return model in current state by name

    > migrator.sql(sql)                             # Run custom SQL
    > migrator.python(func, *args, **kwargs)        # Run python code
    > migrator.create_model(Model)                  # Create a model (could be used as decorator)
    > migrator.remove_model(model, cascade=True)    # Remove a model
    > migrator.add_fields(model, **fields)          # Add fields to a model
    > migrator.change_fields(model, **fields)       # Change fields
    > migrator.remove_fields(model, *field_names, cascade=True)
    > migrator.rename_field(model, old_field_name, new_field_name)
    > migrator.rename_table(model, new_table_name)
    > migrator.add_index(model, *col_names, unique=False)
    > migrator.drop_index(model, *col_names)
    > migrator.add_not_null(model, *field_names)
    > migrator.drop_not_null(model, *field_names)
    > migrator.add_default(model, field_name, default)

"""
# flake8: noqa

import datetime as dt
import peewee as pw

try:
    import playhouse.postgres_ext as pw_pext
except ImportError:
    pass

SQL = pw.SQL


def migrate(migrator, database, fake=False, **kwargs):
    @migrator.create_model
    class Concert(pw.Model):
        songkick_id = pw.IntegerField()
        songkick_uri = pw.CharField()
        linked_artist = pw.ForeignKeyField(migrator.orm['artist'], backref='concerts')
        type = pw.CharField()
        name = pw.CharField()
        start_time = pw.DateTimeField()
        city_name = pw.CharField()
        city_lat = pw.FloatField()
        city_lng = pw.FloatField()
        venue = pw.TextField()
        performances = pw.TextField()
        status = pw.CharField()
        age_restriction = pw.CharField()

    @migrator.create_model
    class UserConcertNotification(pw.Model):
        user = pw.ForeignKeyField(migrator.orm['user'])
        concert = pw.ForeignKeyField(Concert)
        is_sent = pw.BooleanField()


def rollback(migrator, database, fake=False, **kwargs):
    migrator.remove_model(migrator.orm['concert'])
    migrator.remove_model(migrator.orm['userconcertnotification'])
